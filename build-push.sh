#!/usr/bin/env bash

set -o errexit
set -o nounset
set -o pipefail

REGISTRY="quay.io"
REGISTRY_NAMESPACE="cmosetick"
PROJECT_NAME="gitlab-api-tool"
PROJECT_TAG=$(git branch | sed -n -e 's/^\* \(.*\)/\1/p')

PROJECT_PATH="$REGISTRY/$REGISTRY_NAMESPACE/$PROJECT_NAME:$PROJECT_TAG"

docker build . -t $PROJECT_PATH
docker push $PROJECT_PATH
