# GitLab API Tool

Basic python script for adding/removing project variables for multiple GitLab projects at once.

The script takes a JSON file with the following:
* A user's token.
* JSON objects of key/value pairs which will be added/removed in each project.
And a txt file with:
* GitLab project IDs for the projects whose evnironmental variables will be modified.

If attempting to add a variable which already exists, the script will let you know that it exists, and will move on to the others. Similarly, if a variable doesn't exist and you try to delete it, a message will let you know that you're unable to.

# Instructions

Needed:
* Python 3 installed.
* Python GitLab (https://github.com/python-gitlab/python-gitlab)
	* Use `pip3 install python-gitlab`.

1. Download files.
2. Navigate to folder via terminal.
3. Define GitLab token and key/value pairs in **api_data.json**.
4. Define project ID's in **api_prijects.txt**, one project ID per line.
5. Run **gitlab_api_tool.py** via python.
	* `python3 gitlab_api_tool.py`

# Docker Usage Example

1. Copy **api_data.json** to **api_data.json.real** and add your personal token, project ID numbers, and key/value pairs for variables.
2. Mount **api_data.json.real** as a volume named **/api_data.json**
	* `docker run -it --rm -v $PWD/api_data.json.real:/api_data.json quay.io/cmosetick/gitlab-api-tool`

