"""
    Basic python script for adding/removing/modifying project variables for
    multiple GitLab projects at once.

    The script takes a JSON file with the following: a user's token, GitLab
    project IDs, and JSON objects of key/value pairs which will be modified in
    each project.

    If attempting to add a variable which already exists, the script will let
    you know. Similarly, if a variable doesn't exist and you try to delete it, a
    message will let you know.
"""
import requests
import gitlab
import json

# Import txt file with Project ID's.
with open("api_projects.txt", "r") as file:
    project_ids = []
    for line in file:
        project_ids.append(line)

# Import JSON file with data.
with open('api_data.json') as f:
    data = json.load(f)

# Extract token from JSON file.
token = data["token"]

# Private token authentication.
gl = gitlab.Gitlab('https://gitlab.com/', private_token=token)

# Make an API request to create the gl.user object.
gl.auth()

## (ADDING VARIABLES)
if 'env_variables_add' in data:

    # Loop through projects from JSON file.
    for project_id in project_ids:
        print(" ")
        print("[----------------------Adding Variables----------------------]")

        # Get project by ID and store in variable.
        project = gl.projects.get(project_id)
        print("[*] Project name: " + project.name)
        print("[*] Project ID: " + project_id + "\n")

        # List current project variables.
        p_variables = project.variables.list()
        print("[*] Original project variables: ")
        print(*p_variables,sep='\n')

        # Create environmental variables in GitLab project based on key/value
        #   pairs from JSON file.
        for key, value in data["env_variables_add"].items():
            try:
                env_var = project.variables.create({'key': key, 'value': value})
            except gitlab.exceptions.GitlabCreateError:
                print("[!] Variable " + key + " already exists.")

        # Print the modified list of project variables.
        p_variables = project.variables.list()
        print("\n[*] Modified project variables: ")
        print(*p_variables,sep='\n')

## (REMOVING VARIABLES)
if 'env_variables_delete' in data:

    # Loop through projects from JSON file.
    for project_id in project_ids:
        print(" ")
        print("[----------------------Removing Variables--------------------]")

        # Get project by ID and store in variable.
        project = gl.projects.get(project_id)
        print("[*] Project name: " + project.name)
        print("[*] Project ID: " + project_id + "\n")

        # List current project variables.
        p_variables = project.variables.list()
        print("[*] Original project variables: ")
        print(*p_variables,sep='\n')

        # Remove environmental variables from GitLab project based on keys from
        #   JSON file.
        for key, value in data["env_variables_delete"].items():
            try:
                project.variables.delete(key)
            except gitlab.exceptions.GitlabDeleteError:
                print("[!] Variable " + key + " doesn't exist.")

        # Print the modified list of project variables.
        p_variables = project.variables.list()
        print("\n[*] Modified project variables: ")
        print(*p_variables,sep='\n')

print("[------------------------------End---------------------------]")
print("\n")
